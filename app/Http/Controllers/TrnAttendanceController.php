<?php

namespace App\Http\Controllers;

use App\TrnAttendance;
use Illuminate\Http\Request;

class TrnAttendanceController extends Controller
{
    // nullの代わりに使用する時間
    const NULL_HOUR = -1;
    const NULL_MIN = -1;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // ymパラメータ取得
        $yyyymm = (isset($request->ym)) ? $request->ym : date("Ym");

        // Datetimeインスタンス化
        $ymTime = \DateTime::createFromFormat("YmdHis", $yyyymm."01000000");
        // 次月
        $ymNextTime = (clone $ymTime)->add(new \DateInterval("P1M"));
        // 前月
        $ymPrevTime = (clone $ymTime)->sub(new \DateInterval("P1M"));

        // 当月分のデータ取得(配列形式で)
        $data = TrnAttendance
            ::whereBetween('attend_on', [
                $ymTime->format("Y-m-d"),
                $ymNextTime->format("Y-m-d"),
            ])
            ->orderBy('attend_on', 'asc')
            ->get()->all();

        // 当月分の日付配列作成
        $attend = [];
        $currentData = current($data);
        $currentTime = (clone $ymTime);
        while ($currentTime->getTimestamp() < $ymNextTime->getTimestamp()) {
            $d = [];
            if (isset($currentData["attend_on"]) && $currentData["attend_on"] === $currentTime->format("Y-m-d")) {
                // データが存在する
                $d = $currentData;

                // 勤怠時刻をH:i形式に
                if (!is_null($currentData["arrived_at"])) {
                    $aTime = \DateTime::createFromFormat("H:i:s", $currentData["arrived_at"]);
                    $d["arrived_at"] = $aTime->format("H:i");
                }
                if (!is_null($currentData["leaved_at"])) {
                    $lTime = \DateTime::createFromFormat("H:i:s", $currentData["leaved_at"]);
                    $d["leaved_at"] = $lTime->format("H:i");
                }

                // 次のデータを取得
                $currentData = next($data);
            } else {
                // データが存在しない
                $d["id"] = null;
                $d["attend_on"] = $currentTime->format("Y-m-d");
                $d["arrived_at"] = null;
                $d["leaved_at"] = null;
                $d["comment"] = null;
            }
            $attend[] = $d;

            // 1日進める
            $currentTime->add(new \DateInterval("P1D"));
        }

        return view('trn_attendance.index', [
            'yyyy' => $ymTime->format("Y"),
            'mm' => $ymTime->format("m"),
            'yyyymmNext' => $ymNextTime->format("Ym"),
            'yyyymmPrev' => $ymPrevTime->format("Ym"),
            'attend' => $attend,
        ]);
        //return view('debug', ['debug' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  string  $attendOnDate
     * @return \Illuminate\Http\Response
     */
    private function create($attendOnDate)
    {
        return view('trn_attendance.create', [
            "attend_on" => $attendOnDate,
            "arrived_hour" => $this::NULL_HOUR,
            "arrived_min" => $this::NULL_MIN,
            "leaved_hour" => $this::NULL_HOUR,
            "leaved_min" => $this::NULL_MIN,
            "comment" => null,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TODO バリデーション

        // データ保存
        $trnAttendance = new TrnAttendance();
        $trnAttendance->attend_on = $request->attend_on;
        $trnAttendance->arrived_at = $this->makeTimeString($request->arrived_hour, $request->arrived_min);
        $trnAttendance->leaved_at = $this->makeTimeString($request->leaved_hour, $request->leaved_min);
        $trnAttendance->comment = $request->comment;
        $trnAttendance->save();

        return redirect(route("top"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TrnAttendance  $trnAttendance
     * @return \Illuminate\Http\Response
     */
    public function show(TrnAttendance $trnAttendance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TrnAttendance  $trnAttendance
     * @return \Illuminate\Http\Response
     */
    public function edit(TrnAttendance $trnAttendance)
    {
        // 勤怠時刻を時と分に分ける
        $aTime = \DateTime::createFromFormat('H:i:s', $trnAttendance->getAttribute("arrived_at"));
        $lTime = \DateTime::createFromFormat('H:i:s', $trnAttendance->getAttribute("leaved_at"));

        $aHour = $this::NULL_HOUR;
        $aMin = $this::NULL_MIN;
        $lHour = $this::NULL_HOUR;
        $lMin = $this::NULL_MIN;

        if ($aTime) {
            $aHour = intval($aTime->format("H"));
            $aMin = intval($aTime->format("i"));
        }

        if ($lTime) {
            $lHour = intval($lTime->format("H"));
            $lMin = intval($lTime->format("i"));
        }

        return view('trn_attendance.edit', [
            "id" => $trnAttendance->getAttribute("id"),
            "attend_on" => $trnAttendance->getAttribute("attend_on"),
            "arrived_hour" => $aHour,
            "arrived_min" => $aMin,
            "leaved_hour" => $lHour,
            "leaved_min" => $lMin,
            "comment" => $trnAttendance->getAttribute("comment"),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TrnAttendance  $trnAttendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TrnAttendance $trnAttendance)
    {
        //TODO バリデーション

        // データ保存
        $trnAttendance->arrived_at = $this->makeTimeString($request->arrived_hour, $request->arrived_min);
        $trnAttendance->leaved_at = $this->makeTimeString($request->leaved_hour, $request->leaved_min);
        $trnAttendance->comment = $request->comment;
        $trnAttendance->save();

        return redirect(route("top"));
    }

    /**
     * 新規登録画面か編集画面かの判定
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $attendOnDate
     * @return \Illuminate\Http\Response
     */
    public function set(Request $request, $attendOnDate)
    {
        // URLバリデーション
        if (!$this->checkDateFormat($attendOnDate)) {
            return redirect(route("top"));
        }

        // 該当データが存在するか確認
        $data = TrnAttendance::where('attend_on', $attendOnDate)->get();

        if ($data->count() === 0) {
            // 新規登録画面へ
            return $this->create($attendOnDate);
        } else {
            // 編集画面へ
            return $this->edit($data->first());
        }
    }

    /**
     * 出勤処理
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $attendOnDate
     * @return \Illuminate\Http\Response
     */
    public function arrive(Request $request, $attendOnDate)
    {
        // URLバリデーション
        if (!$this->checkDateFormat($attendOnDate)) {
            return redirect(route("top"));
        }

        // リクエストデータに出勤時刻設定
        $request->attend_on = $attendOnDate;
        $request->arrived_hour = date("H");
        $request->arrived_min = date("i");
        $request->leaved_hour = "null";
        $request->leaved_min = "null";

        // 該当データが存在するか確認
        $data = TrnAttendance::where('attend_on', $attendOnDate)->get();
        if ($data->count() === 0) {
            // 新規登録
            return $this->store($request);
        } else {
            // データ更新
            return $this->update($request, $data->first());
        }
    }

    /**
     * 退勤処理
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $attendOnDate
     * @return \Illuminate\Http\Response
     */
    public function leave(Request $request, $attendOnDate)
    {
        // URLバリデーション
        if (!$this->checkDateFormat($attendOnDate)) {
            return redirect(route("top"));
        }

        // リクエストデータに退勤時刻設定
        $request->leaved_hour = date("H");
        $request->leaved_min = date("i");

        // 該当データが存在するか確認
        $data = TrnAttendance::where('attend_on', $attendOnDate)->get();

        if ($data->count() === 0) {
            // データは存在してなければいけない
            return redirect(route("top"));
        } else {
            $trnAttendance = $data->first();
            $aTime = \DateTime::createFromFormat("H:i:s", $trnAttendance->arrived_at);
            // リクエストデータに出勤時刻設定
            $request->arrived_hour = $aTime->format("H");
            $request->arrived_min = $aTime->format("i");

            // データ更新
            return $this->update($request, $trnAttendance);
        }

    }

    /**
     * yyyy-mm-dd形式をバリデーション
     *
     * @param  string  $date
     * @return true / false
     */
    private function checkDateFormat ($date)
    {
        if (!preg_match('/^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/', $date, $matches)) {
            return false;
        }
        return checkdate((int)$matches[2], (int)$matches[3], (int)$matches[1]);
    }

    /**
     * hh:mi形式を作成
     *
     * @param  int  $hour
     * @param  int  $min
     * @return string 時刻文字列("H:i") or null
     */
    private function makeTimeString ($hour, $min)
    {
        if ($hour === "null" || $min === "null") {
            return null;
        }

        $time = str_pad($hour, 2, "0", STR_PAD_LEFT)
            . ":" . str_pad($min, 2, "0", STR_PAD_LEFT);

        return $time;
    }
}
