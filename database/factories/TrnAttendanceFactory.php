<?php

use Faker\Generator as Faker;

$factory->define(App\TrnAttendance::class, function (Faker $faker) {
    return [
        'arrived_at' => '10:00:00',
        'leaved_at' => '19:00:00',
        'comment' => '',
        'created_at' => time(),
        'updated_at' => time(),
    ];
});
