<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrnAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trn_attendances', function (Blueprint $table) {
            $table->increments('id');
            $table->date('attend_on')->comment('日付')->unique();
            $table->time('arrived_at')->nullable()->comment('出社時刻');
            $table->time('leaved_at')->nullable()->comment('退社時刻');
            $table->string('comment')->nullable()->comment('コメント');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trn_attendances');
    }
}
