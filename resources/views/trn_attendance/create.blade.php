@extends('layouts.app')

@section('content')
    <form
        action="{{ route('attend.store') }}"
        method="post">
        @csrf
        @method('POST')
        @include('trn_attendance.form')
    </form>
@endsection
