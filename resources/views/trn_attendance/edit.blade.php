@extends('layouts.app')

@section('content')
    <form
        action="{{ route('attend.update', ['trn_attendance' => $id]) }}"
        method="post">
        @csrf
        @method('PUT')
        @include('trn_attendance.form')
    </form>
@endsection
