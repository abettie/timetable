<input type="hidden" name="attend_on" value="{{ $attend_on }}">
<div class="row">
    <div class="col">
        <h1 class="form_title">{{ $attend_on }}の勤怠</h1>
    </div>
</div>
<div class="row">
    <div class="col-md">
        <label for="arrived_hour">出勤：</label>
        <select name="arrived_hour" id="arrived_hour">
            <option value="null"> -- </option>
            @for($i=0; $i<=23; $i++)
                <option value="{{ $i }}" @if($i === (int)$arrived_hour) selected @endif>
                    {{ str_pad($i, 2, "0", STR_PAD_LEFT) }}
                </option>
            @endfor
        </select>
        <select name="arrived_min" id="arrived_min">
            <option value="null"> -- </option>
            @for($i=0; $i<=59; $i++)
                <option value="{{ $i }}" @if($i === (int)$arrived_min) selected @endif>
                    {{ str_pad($i, 2, "0", STR_PAD_LEFT) }}
                </option>
            @endfor
        </select>
    </div>
    <div class="col-md">
        <label for="leaved_hour">退勤：</label>
        <select name="leaved_hour" id="leaved_hour">
            <option value="null"> -- </option>
            @for($i=0; $i<=23; $i++)
                <option value="{{ $i }}" @if($i === (int)$leaved_hour) selected @endif>
                    {{ str_pad($i, 2, "0", STR_PAD_LEFT) }}
                </option>
            @endfor
        </select>
        <select name="leaved_min" id="leaved_min">
            <option value="null"> -- </option>
            @for($i=0; $i<=59; $i++)
                <option value="{{ $i }}" @if($i === (int)$leaved_min) selected @endif>
                    {{ str_pad($i, 2, "0", STR_PAD_LEFT) }}
                </option>
            @endfor
        </select>
    </div>
    <div class="col-md">
        <label for="comment">備考：</label>
        <input type="text" name="comment" id="comment" value="{{ $comment }}">
    </div>
</div>
<div class="row">
    <div class="col">
        <input type="submit" value="送信">
    </div>
</div>
