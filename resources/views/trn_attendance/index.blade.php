@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h1 class="text-center">{{ $yyyy }}年{{ $mm }}月</h1>
            <ul class="pagination justify-content-between">
                <li class="page-item">
                    <a class="page-link" href="{{ route('attend.index') }}?ym={{ $yyyymmPrev }}">&lt; Prev</a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="{{ route('attend.index') }}?ym={{ $yyyymmNext }}">Next &gt;</a>
                </li>
            </ul>
            <table class="attendance table table-sm table-striped mx-auto">
                <tr>
                    <th>日付</th>
                    <th>出勤</th>
                    <th>退勤</th>
                    <th>備考</th>
                    <th>編集</th>
                </tr>
                @foreach($attend as $a)
                    <tr>
                        <td>{{ $a["attend_on"] }}</td>
                        <td>
                            @if($a["attend_on"] == date("Y-m-d") && is_null($a["arrived_at"]))
                                <a class="btn btn-primary" href="/attend/arrive/{{ $a["attend_on"] }}">出勤</a>
                            @else
                                {{ $a["arrived_at"] }}
                            @endif
                        </td>
                        <td>
                            @if($a["attend_on"] == date("Y-m-d") && is_null($a["leaved_at"]) && !is_null($a["arrived_at"]))
                                <a class="btn btn-primary" href="/attend/leave/{{ $a["attend_on"] }}">退勤</a>
                            @else
                                {{ $a["leaved_at"] }}
                            @endif
                        </td>
                        <td>{{ $a["comment"] }}</td>
                        <td>
                            <a class="btn btn-secondary" href="/attend/set/{{ $a["attend_on"] }}">編集</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
