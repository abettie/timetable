<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TrnAttendanceController@index')->name('top');

Route::resource('attend', 'TrnAttendanceController', ['only' => ['index', 'update', 'store']])->parameters([
    'attend' => 'trn_attendance',
]);

Route::get('attend/arrive/{date}', 'TrnAttendanceController@arrive')->name('arrive');
Route::get('attend/leave/{date}', 'TrnAttendanceController@leave')->name('leave');
Route::get('attend/set/{date}', 'TrnAttendanceController@set')->name('set');
